#include "Object.h"
#include <iostream>


#define PI (4.0*atan(1.0))


/////////////////////////////////////////////////////////////////////////
// public constructor/destructor

Object::Object(const char* path)
{
	vModls = new std::vector<Modl*>;

	// open file
	fsMesh.open(path, std::ios::in | std::ios::binary);

	if (!fsMesh.is_open())
		throw std::invalid_argument(std::string("file not found: ") += path);

	// jump to file size information
	fsMesh.seekg(4);

	std::uint32_t tempFileSize;
	std::list<ChunkHeader*> tempMainChunks;

	// get all chunks under HEDR
	fsMesh.read(reinterpret_cast<char*>(&tempFileSize), sizeof(tempFileSize));
	loadChunks(tempMainChunks, fsMesh.tellg(), tempFileSize);

	// evaluate HEDR subchunks (= find MSH2)
	for (std::list<ChunkHeader*>::iterator it = tempMainChunks.begin(); it != tempMainChunks.end(); it++)
	{
		if (!strcmp("MSH2", (*it)->name))
		{
			// get all subchunks
			std::list<ChunkHeader*> tempMsh2Chunks;
			loadChunks(tempMsh2Chunks, (*it)->position, (*it)->size);

			// evaluate MSH2 subchunks
			analyseMsh2Chunks(tempMsh2Chunks);

			// clean up
			while (!tempMsh2Chunks.empty())
			{
				ChunkHeader* tempCursor = tempMsh2Chunks.front();
				tempMsh2Chunks.pop_front();
				delete tempCursor;
			}
			continue;
		}
	}

	// clean up
	while (!tempMainChunks.empty())
	{
		ChunkHeader* tempCursor = tempMainChunks.front();
		tempMainChunks.pop_front();
		delete tempCursor;
	}

	// close file
	fsMesh.close();
}

Object::~Object()
{
	// clear texture list
	vTextures.clear();
}


/////////////////////////////////////////////////////////////////////////
// private functions

void Object::loadChunks(std::list<ChunkHeader*>& destination, std::streampos start, const std::uint32_t end)
{
	// jump to first chunk
	fsMesh.seekg(start);

	do
	{
		ChunkHeader* tempHeader = new ChunkHeader();

		fsMesh.read(reinterpret_cast<char*>(&tempHeader->name[0]), sizeof(tempHeader->name) - 1);
		fsMesh.read(reinterpret_cast<char*>(&tempHeader->size), sizeof(tempHeader->size));
		tempHeader->position = fsMesh.tellg();

		destination.push_back(tempHeader);

		fsMesh.seekg(tempHeader->size, std::ios_base::cur);

		// reached end
		if (fsMesh.tellg() - start == end)
			break;

		// error. Maybe the size information is corrupted
		if (!fsMesh.good())
		{
			std::cout << "WARNING: corrupted file. Trying to continue" << std::endl;
			fsMesh.clear();
			break;
		}

	} while (true);
}

void Object::analyseMsh2Chunks(std::list<ChunkHeader*>& chunkList)
{
	for (auto& it : chunkList)
	{
		if (!strcmp("SINF", it->name))
		{
			std::list<ChunkHeader*> tempSinfChunks;
			loadChunks(tempSinfChunks, it->position, it->size);

			// evaluate MATL subchunks
			for (auto& it : tempSinfChunks)
			{
				if (!strcmp("BBOX", it->name))
				{
					fsMesh.seekg(it->position);

					// read in the quaternion
					for (int i = 0; i < 4; i++)
						fsMesh.read(reinterpret_cast<char*>(&boundingBox.quaternion[i]), sizeof(float));

					//read in the center
					for (int i = 0; i < 3; i++)
						fsMesh.read(reinterpret_cast<char*>(&boundingBox.center[i]), sizeof(float));

					//read in the extents
					for (int i = 0; i < 3; i++)
						fsMesh.read(reinterpret_cast<char*>(&boundingBox.extents[i]), sizeof(float));
				}
			}

			for (ChunkHeader* it : tempSinfChunks)
				delete it;
		}

		else if (!strcmp("MATL", it->name))
		{
			// "useless" information how many MATD follow
			fsMesh.seekg(it->position);
			fsMesh.seekg(sizeof(std::uint32_t), std::ios_base::cur);

			// get all MATD from MATL list
			std::list<ChunkHeader*> tempMatlChunks;
			loadChunks(tempMatlChunks, fsMesh.tellg(), it->size - 4);

			// evaluate MATL subchunks
			for (auto& it : tempMatlChunks)
			{
				// This shouldn't be anything else than MATD
				if (!strcmp("MATD", it->name))
				{
					// get all subchunks from MATD
					std::list<ChunkHeader*> tempMatdChunks;
					loadChunks(tempMatdChunks, it->position, it->size);

					vTextures.push_back("");

					// analyse MATD subchunks
					analyseMatdChunks(tempMatdChunks);

					// clean up
					while (!tempMatdChunks.empty())
					{
						ChunkHeader* tempCursor = tempMatdChunks.front();
						tempMatdChunks.pop_front();
						delete tempCursor;
					}
				}
			}

			// clean up
			while (!tempMatlChunks.empty())
			{
				ChunkHeader* tempCursor = tempMatlChunks.front();
				tempMatlChunks.pop_front();
				delete tempCursor;
			}
		}

		else if (!strcmp("MODL", it->name))
		{
			Modl* tempModl = new Modl;

			// get all subchunks
			std::list<ChunkHeader*> tempChunks;
			loadChunks(tempChunks, it->position, it->size);

			// evaluate MODL subchunks
			analyseModlChunks(tempModl, tempChunks);

			//clean up
			while (!tempChunks.empty())
			{
				ChunkHeader* tempCursor = tempChunks.front();
				tempChunks.pop_front();
				delete tempCursor;
			}

			// save Model data
			vModls->push_back(tempModl);
		}
	}
}

void Object::analyseMatdChunks(std::list<ChunkHeader*>& chunkList)
{
	for (auto& it : chunkList)
	{
		//TX1D, TX2D, TX3D
		if (!strcmp("TX0D", it->name))
		{
			fsMesh.seekg(it->position);
			char* buffer = new char[it->size + 1];
			*buffer = { 0 };
			fsMesh.read(buffer, it->size);
			vTextures.back() = buffer;
			delete[] buffer;
		}
	}
}

void Object::analyseModlChunks(Modl* dataDestination, std::list<ChunkHeader*>& chunkList)
{
	for (auto& it : chunkList)
	{
		if (!strcmp("MTYP", it->name))
		{
			fsMesh.seekg(it->position);
			std::uint32_t tempType;
			fsMesh.read(reinterpret_cast<char*>(&tempType), sizeof(tempType));
			dataDestination->type = (Mtyp)tempType;
		}

		else if (!strcmp("PRNT", it->name))
		{
			fsMesh.seekg(it->position);
			char* buffer = new char[it->size];
			*buffer = { 0 };
			fsMesh.read(buffer, it->size); 
			dataDestination->parent = buffer;
			delete[] buffer;
		}

		else if (!strcmp("NAME", it->name))
		{
			fsMesh.seekg(it->position);
			char* buffer = new char[it->size];
			*buffer = { 0 };
			fsMesh.read(buffer, it->size);
			dataDestination->name = buffer;
			delete[] buffer;
		}

		else if (!strcmp("FLGS", it->name))
		{
			fsMesh.seekg(it->position);
			fsMesh.read(reinterpret_cast<char*>(&dataDestination->renderFlags), sizeof(dataDestination->renderFlags));
		}

		else if (!strcmp("TRAN", it->name))
		{
			float tempScale[3];
			float tempRotation[4];
			float tempTrans[3];

			fsMesh.seekg(it->position);

			for(int i = 0; i < 3; i++)
				fsMesh.read(reinterpret_cast<char*>(&tempScale[i]), sizeof(float));

			for (int i = 0; i < 4; i++)
				fsMesh.read(reinterpret_cast<char*>(&tempRotation[i]), sizeof(float));

			quat2eul(tempRotation[0], tempRotation[1], tempRotation[2], tempRotation[3]);

			for (int i = 0; i < 3; i++)
				fsMesh.read(reinterpret_cast<char*>(&tempTrans[i]), sizeof(float));

			dataDestination->m4x4Translation = glm::scale(
				dataDestination->m4x4Translation,
				glm::vec3(tempScale[0], tempScale[1], tempScale[2])
			);

			dataDestination->m4x4Translation = glm::translate(
				dataDestination->m4x4Translation,
				glm::vec3(tempTrans[0], tempTrans[1], tempTrans[2])
			);

			dataDestination->m4x4Translation = glm::rotate(
				dataDestination->m4x4Translation,
				tempRotation[0],
				glm::vec3(1, 0, 0)
			);

			dataDestination->m4x4Translation = glm::rotate(
				dataDestination->m4x4Translation,
				tempRotation[1],
				glm::vec3(0, 1, 0)
			);

			dataDestination->m4x4Translation = glm::rotate(
				dataDestination->m4x4Translation,
				tempRotation[2],
				glm::vec3(0, 0, 1)
			);

		}

		else if (!strcmp("GEOM", it->name))
		{
			// get all subchunks
			std::list<ChunkHeader*> tempGeomChunks;
			loadChunks(tempGeomChunks, it->position, it->size);

			// evaluate GEOM subchunks
			analyseGeomChunks(dataDestination, tempGeomChunks);

			// clean up
			while (!tempGeomChunks.empty())
			{
				ChunkHeader* tempCursor = tempGeomChunks.front();
				tempGeomChunks.pop_front();
				delete tempCursor;
			}
		}
	}
}

void Object::analyseGeomChunks(Modl * dataDestination, std::list<ChunkHeader*>& chunkList)
{
	for (auto& it : chunkList)
	{
		if (!strcmp("SEGM", it->name))
		{
			// get all subchunks
			std::list<ChunkHeader*> tempSegmChunks;
			loadChunks(tempSegmChunks, it->position, it->size);

			// evaluate SEGM subchunks
			analyseSegmChunks(dataDestination, tempSegmChunks);

			// clean up
			while (!tempSegmChunks.empty())
			{
				ChunkHeader* tempCursor = tempSegmChunks.front();
				tempSegmChunks.pop_front();
				delete tempCursor;
			}
			continue;
		}
		
		if (!strcmp("CLTH", it->name))
		{
			// get all subchunks
			std::list<ChunkHeader*> tempClthChunks;
			loadChunks(tempClthChunks, it->position, it->size);

			// evaluate CLTH subchunks
			analyseClthChunks(dataDestination, tempClthChunks);

			// clean up
			while (!tempClthChunks.empty())
			{
				ChunkHeader* tempCursor = tempClthChunks.front();
				tempClthChunks.pop_front();
				delete tempCursor;
			}
			continue;
		}
	}
}

void Object::analyseSegmChunks(Modl * dataDestination, std::list<ChunkHeader*>& chunkList)
{
	Segment* tempData = new Segment;

	for (auto& it : chunkList)
	{
		if (!strcmp("MATI", it->name))
		{
			fsMesh.seekg(it->position);
			fsMesh.read(reinterpret_cast<char*>(&tempData->textureIndex), sizeof(tempData->textureIndex));
		}

		else if (!strcmp("POSL", it->name))
		{
			readVertex(tempData, it->position);
		}

		/*else if (!strcmp("NRML", it->name))
		{
			fsMesh.seekg(it->position);
			std::uint32_t tempSize;
			fsMesh.read(reinterpret_cast<char*>(&tempSize), sizeof(tempSize));
			// List of normals
			// long int - 4 - number of normal vectores stored in this list
			// float[3][] - 12 each - UVW vector for each vertex
		}*/

		else if (!strcmp("UV0L", it->name))
		{
			readUV(tempData, it->position);
		}

		else if (!strcmp("STRP", it->name))
		{
			// don't get null, bone, shadowMesh and hidden mesh indices
			if (dataDestination->type == null ||
				dataDestination->type == bone ||
				dataDestination->type == shadowMesh ||
				dataDestination->renderFlags == 1)
				continue;

			// jump to the data section and read the size;
			std::uint32_t tempSize;
			fsMesh.seekg(it->position);
			fsMesh.read(reinterpret_cast<char*>(&tempSize), sizeof(tempSize));

			int highBitCount(0);
			std::vector<uint32_t> tempPoly; // = new std::vector<uint32_t>;
			
			for (unsigned int i = 0; i < tempSize; i++)
			{
				// ReadData
				std::uint16_t tempValue;
				fsMesh.read(reinterpret_cast<char*>(&tempValue), sizeof(std::uint16_t));

				// Check for highbit
				if (tempValue >> 15)
				{
					highBitCount++;
					tempValue = (std::uint16_t(tempValue << 1) >> 1);
				}

				tempPoly.push_back((std::uint32_t)tempValue);

				// new Polygon found
				if (highBitCount == 2)
				{
					// reset highBitCount
					highBitCount = 0;

					// remove the last two values..
					std::uint32_t saveData[2];
					for (int i = 0; i < 2; i++)
					{
						saveData[i] = tempPoly.back();
						tempPoly.pop_back();
					}

					// ..and save them in the new vector
					tempData->meshIndices.push_back(tempPoly);

					tempPoly.clear();
					for (int i = 1; i >= 0; i--)
						tempPoly.push_back(saveData[i]);

				}	// if high bit set
				
			}	// for all values

			// save the last values (where no 2 high bits follow);
			tempData->meshIndices.push_back(tempPoly);

			// kick the first element, it's empty as a reason of the algo above;
			tempData->meshIndices.erase(tempData->meshIndices.begin());
		}
	}

	dataDestination->segmLst.push_back(tempData);
}

void Object::analyseClthChunks(Modl * dataDestination, std::list<ChunkHeader*>& chunkList)
{
	Segment* tempData = new Segment;

	for (auto& it : chunkList)
	{
		if (!strcmp("CTEX", it->name))
		{
			fsMesh.seekg(it->position);
			char* buffer = new char[it->size];
			*buffer = { 0 };
			fsMesh.read(buffer, it->size);

			bool tempFound(false);

			for (unsigned int index = 0; index < vTextures.size(); index++)
			{
				if (!strcmp(buffer, vTextures[index].c_str()))
				{
					tempData->textureIndex = index;
					tempFound = true;
					break;
				}
			}

			if (!tempFound)
			{
				vTextures.push_back(std::string(buffer));
				tempData->textureIndex = vTextures.size() - 1;
			}

			delete[] buffer;
		}

		else if (!strcmp("CPOS", it->name))
		{
			readVertex(tempData, it->position);
		}

		else if (!strcmp("CUV0", it->name))
		{
			readUV(tempData, it->position);
		}

		else if (!strcmp("CMSH", it->name))
		{
			// jump to the data section and read the size;
			std::uint32_t tempSize;
			fsMesh.seekg(it->position);
			fsMesh.read(reinterpret_cast<char*>(&tempSize), sizeof(tempSize));

			std::vector<uint32_t> tempPoly;

			// for every triangle..
			for (unsigned int i = 0; i < tempSize * 3; i += 3)
			{
				tempPoly.clear();

				// ..get the 3 indices and save them
				for (int j = 0; j < 3; j++)
				{
					std::uint32_t tempValue;
					fsMesh.read(reinterpret_cast<char*>(&tempValue), sizeof(std::uint32_t));
					tempPoly.push_back(tempValue);
				}
				tempData->meshIndices.push_back(tempPoly);
			}
		}
	}
	dataDestination->segmLst.push_back(tempData);
}

void Object::readVertex(Segment* dataDestination, std::streampos position)
{
	std::uint32_t tempSize;
	fsMesh.seekg(position);
	fsMesh.read(reinterpret_cast<char*>(&tempSize), sizeof(tempSize));

	dataDestination->vertex = new float[tempSize * 3];

	for (unsigned int i = 0; i < tempSize * 3; i++)
		fsMesh.read(reinterpret_cast<char*>(&dataDestination->vertex[i]), sizeof(float));
}

void Object::readUV(Segment* dataDestination, std::streampos position)
{
	std::uint32_t tempSize;
	fsMesh.seekg(position);
	fsMesh.read(reinterpret_cast<char*>(&tempSize), sizeof(tempSize));

	dataDestination->uv = new float[tempSize * 2];

	for (unsigned int i = 0; i < tempSize * 2; i++)
		fsMesh.read(reinterpret_cast<char*>(&dataDestination->uv[i]), sizeof(float));
}

void Object::quat2eul(float &quat0, float &quat1, float & quat2, float &quat3)
{
	float ysqr = quat1 * quat1;
	float t0 = -2.0f * (ysqr + quat2 * quat2) + 1.0f;
	float t1 = +2.0f * (quat0 * quat1 - quat3 * quat2);
	float t2 = -2.0f * (quat0 * quat2 + quat3 * quat1);
	float t3 = +2.0f * (quat1 * quat2 - quat3 * quat0);
	float t4 = -2.0f * (quat0 * quat0 + ysqr) + 1.0f;

	t2 = t2 > 1.0f ? 1.0f : t2;
	t2 = t2 < -1.0f ? -1.0f : t2;

	quat1 = std::asin(t2);
	quat0 = std::atan2(t3, t4);
	quat2 = std::atan2(t1, t0);

}


/////////////////////////////////////////////////////////////////////////
// public getter

std::vector<Modl*>* Object::getModels() const
{
	return vModls;
}

std::vector<std::string> Object::getTextureList() const
{
	return vTextures;
}

Bbox Object::getBoundgBox() const
{
	return boundingBox;
}


/////////////////////////////////////////////////////////////////////////
// public functions

