#pragma once

extern void windowResize(GLFWwindow * window, int width, int height);

extern void mouseButton(GLFWwindow *window, int button, int action, int mod);

extern void mouseMove(GLFWwindow *window, double xpos, double ypos);

extern void mouseWheel(GLFWwindow *window, double xoffset, double yoffset);

extern void keyPress(GLFWwindow *window, int key, int scancode, int action, int mods);

extern void dragNdrop(GLFWwindow* window, int count, const char** paths);