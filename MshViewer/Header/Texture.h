#pragma once
#include <vector>


class TextureTGA
{
public:
	TextureTGA(const char* filePath);
	~TextureTGA();

private:
	std::vector<std::uint8_t>* vui8Pixels;
	std::uint32_t ui32BpP;
	std::uint32_t ui32Width;
	std::uint32_t ui32Height;

public:
	std::vector<std::uint8_t>* getData() const;
	bool hasAlpha() const;
	std::uint32_t getWidth() const;
	std::uint32_t getHeight() const;
};
