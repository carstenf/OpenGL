#pragma once
#include <vector>
#include <list>
#include <fstream>
#include <string>
#include <glm\gtc\matrix_transform.hpp>

enum Mtyp {
	null,
	dynamicMesh,
	cloth,
	bone,
	staticMesh,
	shadowMesh = 6
};

struct Bbox {
	float quaternion[4];
	float center[3];
	float extents[3];
};

struct ChunkHeader {
	char name[5];
	std::uint32_t size;
	std::streampos position;
};

struct Segment {
	std::uint32_t textureIndex	= 0;
	float* vertex				= nullptr;
	float* uv					= nullptr;
	std::vector<std::vector<std::uint32_t>> meshIndices;	// indices into vertex array
};

struct Modl {
	std::string name			= "";
	std::string parent			= "";
	Mtyp type					= null;
	std::int32_t renderFlags	= -1;
	glm::mat4 m4x4Translation	= glm::mat4(1.0f);
	std::vector<Segment*> segmLst;
};


class Object
{
public:
	Object(const char* path);
	~Object();

private:

	std::vector<Modl*>* vModls;
	std::fstream fsMesh;
	std::vector<std::string> vTextures;
	Bbox boundingBox;

private:
	void loadChunks(std::list<ChunkHeader*> &destination, std::streampos start, const std::uint32_t end);
	void analyseMsh2Chunks(std::list<ChunkHeader*> &chunkList);
	void analyseMatdChunks(std::list<ChunkHeader*> &chunkList);
	void analyseModlChunks(Modl* dataDestination, std::list<ChunkHeader*> &chunkList);
	void analyseGeomChunks(Modl* dataDestination, std::list<ChunkHeader*> &chunkList);
	void analyseSegmChunks(Modl* dataDestination, std::list<ChunkHeader*> &chunkList);
	void analyseClthChunks(Modl* dataDestination, std::list<ChunkHeader*> &chunkList);
	void readVertex(Segment* dataDestination, std::streampos position);
	void readUV(Segment* dataDestination, std::streampos position);
	void quat2eul(float &quat0, float &quat1, float &quat2, float &quat3);
	

public:
	std::vector<Modl*>* getModels() const;
	std::vector<std::string> getTextureList() const;
	Bbox getBoundgBox() const;
};
