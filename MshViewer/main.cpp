#ifndef _DEBUG
#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )
#endif // DEBUG

#include "OpenGLController.h"

int main(int argc, char** argv)
{
	OpenGLController* scene = NULL;

	if (argc == 3)
	{
		int major = atoi(argv[1]);
		int minor = atoi(argv[2]);
		scene = OpenGLController::getInstance(major, minor);
	}
	else
		scene = OpenGLController::getInstance();

#ifdef _DEBUG
	scene->loadMsh("..\\Release\\Msh\\triClothMan.msh");
#endif // DEBUG

	do {
		scene->updateScene();
	} while (!glfwWindowShouldClose(scene->getWindow()));

	delete scene;

	return 0;
}