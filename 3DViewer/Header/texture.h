#pragma once
#include <vector>


class TextureTGA
{
public:
	TextureTGA(const char* filePath);
	~TextureTGA();

private:
	std::vector<std::uint8_t> vui8Pixels;
	bool bCompressed;
	std::uint32_t ui32IDLength;
	bool bColorTabel;
	std::uint32_t ui32PicType;
	std::uint32_t ui32PaletteBegin;
	std::uint32_t ui32PaletteLength;
	std::uint32_t ui32PaletteBpP;
	std::uint32_t ui32Width;
	std::uint32_t ui32Height;
	std::uint32_t ui32Size;
	std::uint32_t ui32BpP;
	std::uint32_t ui32Attribut;

public:
	std::vector<std::uint8_t> getData() const;
	bool hasAlpha() const;
	std::uint32_t getWidth() const;
	std::uint32_t getHeight() const;
};
