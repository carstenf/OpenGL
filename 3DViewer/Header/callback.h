#pragma once
#include <iostream>
#include <gl\glew.h>
#include <gl\glfw3.h>
#include "setup.h"

#define GLFW_KEY_PLUS_GER GLFW_KEY_RIGHT_BRACKET
#define GLFW_KEY_MINUS_GER GLFW_KEY_SLASH

struct {
	double posX;
	double posY;
	bool leftHold;
	bool middleHold;
	bool rightHold;
	double speed = 1;
} mouse;


void windowResize(GLFWwindow * window, int width, int height)
{
	monitor.witdh = width;
	monitor.heigh = height;

	glViewport(0, 0, width, height);
}

void mouseButton(GLFWwindow *window, int button, int action, int mod)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		mouse.leftHold = true;
		glfwGetCursorPos(window, &mouse.posX, &mouse.posY);
	}
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		mouse.leftHold = false;
	}

	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		mouse.rightHold = true;
		glfwGetCursorPos(window, &mouse.posX, &mouse.posY);
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		mouse.rightHold = false;
	}
}

void mouseMove(GLFWwindow *window, double xpos, double ypos)
{
	if (mouse.leftHold)
	{
		monitor.rotX += static_cast<float>((ypos - mouse.posY) * 0.01 * mouse.speed);
		monitor.rotY += static_cast<float>((xpos - mouse.posX) * 0.01 * mouse.speed);

		mouse.posX = xpos;
		mouse.posY = ypos;
	}

	if (mouse.rightHold)
	{
		monitor.transX -= (xpos - mouse.posX) * 0.01 * mouse.speed;
		monitor.transY += (ypos - mouse.posY) * 0.01 * mouse.speed;
		
		mouse.posX = xpos;
		mouse.posY = ypos;
	}
}

void mouseWheel(GLFWwindow *window, double xoffset, double yoffset)
{
	monitor.transZ -= yoffset * 0.5 * mouse.speed;
}

void keyPress(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS || action == GLFW_REPEAT)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, GLFW_TRUE);
			break;
		case GLFW_KEY_MINUS_GER: case GLFW_KEY_KP_SUBTRACT:
			mouse.speed -= 0.1;
			if (mouse.speed < 0.1)
				mouse.speed = 0;
			break;
		case GLFW_KEY_PLUS_GER: case GLFW_KEY_KP_ADD:
			mouse.speed += 0.1;
			break;
		case GLFW_KEY_A:
			std::cout << mouse.speed << std::endl;
				break;
		default:
			std::cout << key << " " << scancode << std::endl;
			break;
		}
	}
}
