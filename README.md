# MshViewer

I started to learn OpenGL. Therefore i decided to implement a .msh viewer for SWBFII assets, since the old one
is not very good. But we don't have the source, so i start from the beginning.
Why OpenGL for a viewer?
well that way i can display specular and normal maps and maybe show animations, too.

### The Team
So far it is just me. If you wanna help me, let me know :D

### Licence
Feel free to use my code the way you like. But remember i used some public libraries. Make sure you read their
licence, too.


### Notes
not continued at the moment. Last commit 2021-08-28

Update 2024-03-16